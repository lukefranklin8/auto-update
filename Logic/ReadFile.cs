﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class ReadFile
    {
        private string Username = "LAFranklin";
        private bool AddUsername = true;
        private bool BrowseSubDirectory = false;
        private string DefaultCurrency = "USD";
        private string Location
        {
            get
            {
                if (AddUsername)
                    return "C:\\Users\\Luke\\Desktop\\Poker\\Tournement Summary\\" + Username + "\\";
                else
                    return "C:\\Users\\Luke\\Desktop\\Poker\\Tournement Summary\\";
            }
        }
        private string ErrorLocation
        {
            get
            {
                return Location + "Errored\\";
            }
        }
        private string ProcessedLocation
        {
            get
            {
                return Location + "Processed\\";
            }
        }


        public ReadFile()
        {
            ProcessDirectory(Location);
        }
        // Process all files in the directory passed in, recurse on any directories 
        // that are found, and process the files they contain.
        public void ProcessDirectory(string targetDirectory)
        {
            // Process the list of files found in the directory.
            string[] fileEntries = Directory.GetFiles(targetDirectory);
            foreach (string fileName in fileEntries)
                ProcessFile(fileName);

            if (BrowseSubDirectory)
            {
                // Recurse into subdirectories of this directory.
                string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
                foreach (string subdirectory in subdirectoryEntries)
                    ProcessDirectory(subdirectory);
            }
        }

        // Insert logic for processing found files here.
        public void ProcessFile(string path)
        {
            Console.WriteLine("Processed file '{0}'.", path);
            int counter = 0;
            string line;
            bool error = false;

            // Read the file and display it line by line.
            using (StreamReader file = new StreamReader(path))
            {
                using (var db = new AutoImportEntities())
                {
                    try
                    {
                        Tournement t = new Tournement();
                        TargetTournement tt = new TargetTournement();
                        while ((line = file.ReadLine()) != null)
                        {
                            if (line.Contains("PokerStars Tournament"))
                            {
                                string[] tourneyType = line.Replace("PokerStars Tournament ", "").Split(',');
                                t.Tid = Convert.ToDecimal(tourneyType[0].Substring(1)); // starts with hash symbol
                                Console.WriteLine("Id - " + t.Tid);
                                Console.WriteLine("Type - " + tourneyType[1].Trim());
                            }
                            // when tournment is a paid for event.
                            else if (line.Contains("Buy-In:") && !line.Contains("Target Tournament "))
                            {
                                line = line.Replace("Buy-In: ", "");
                                if (line.Contains("/"))
                                {
                                    string[] buyinRake = line.Split('/');
                                    string[] rakeCurrency = buyinRake[1].Split(' ');
                                    t.Buyin = Convert.ToDecimal(buyinRake[0].Substring(1)); // starts with doller sign
                                    t.Rake = Convert.ToDecimal(rakeCurrency[0].Substring(1)); // starts with doller sign
                                    t.Currency = rakeCurrency[1];
                                    Console.WriteLine("Buyin - " + t.Buyin);
                                    Console.WriteLine("Rake - " + t.Rake);
                                    Console.WriteLine("Currency - " + t.Currency);
                                }
                                else
                                {
                                    string[] rakeCurrency = line.Split(' ');
                                    t.Buyin = Convert.ToDecimal(rakeCurrency[0]);
                                    t.Currency = rakeCurrency[1];
                                    Console.WriteLine("Buyin - " + t.Buyin);
                                    Console.WriteLine("Currency - " + t.Currency);
                                }
                            }
                            // when tournement is a ticketed event.
                            else if (line.Contains("Target Tournament "))
                            {
                                string raw = line.Replace("Target Tournament #", "");
                                string[] splitup = raw.Split(' ');
                                tt.TargetTid = Convert.ToDecimal(splitup[0]);
                                t.IsTicketed = true;
                                Console.WriteLine("TargetTournement - " + tt.TargetTid);
                            }
                            else if (line.Contains("Freeroll"))
                            {
                                t.IsFreeroll = true;
                            }
                            else if (line.Contains(" players"))
                            {
                                t.Players = Convert.ToInt32(line.Replace(" players", ""));
                                Console.WriteLine("Players - " + t.Players);
                            }
                            else if (line.Contains("Total Prize Pool:") && !line.Contains("Ticket"))
                            {
                                string[] amountCurrency = line.Replace("Total Prize Pool: ", "").Split(' ');
                                t.TotalPrizePool = Convert.ToDecimal(amountCurrency[0].Substring(1));  // starts with doller sign 
                                Console.WriteLine("Total Prize Pool - " + t.TotalPrizePool);
                                Console.WriteLine("Currency - " + amountCurrency[1]);

                                // added for free tourneys to make sure a currency is set. 
                                if (String.IsNullOrEmpty(t.Currency))
                                    t.Currency = amountCurrency[1];
                            }
                            else if (line.Contains(Username))
                            {
                                string[] position = line.Split(':');
                                Console.WriteLine("Finish Position - " + position[0].Trim());

                                string[] winnings = position[1].Split(',');
                                if (winnings[1].Contains("("))
                                {
                                    Console.WriteLine("Total Winnings - " + winnings[1].Trim());
                                    if (winnings[1] == "  (qualified for the target tournament)")
                                    {
                                        tt.Qualified = true;
                                        t.TotalWinnings = 0; // due to the winnings not being cash the winnings is 0
                                    }
                                    else
                                    {
                                        string[] winningsPercent = winnings[1].Trim().Split(' ');
                                        t.TotalWinnings = Convert.ToDecimal(winningsPercent[0].Substring(1)); // starts with doller sign 
                                    }

                                }

                                else
                                {
                                    Console.WriteLine("Total Winnings - " + "0.00");
                                    Convert.ToDecimal("0.00");
                                }
                                t.FinishPosition = Convert.ToInt32(position[0].Trim());
                                break;
                            }
                            counter++;
                        }
                        if (String.IsNullOrEmpty(t.Currency))
                            t.Currency = DefaultCurrency;
                        t.PlayedDate = File.GetLastWriteTime(path);
                        db.Tournements.Add(t);
                        db.SaveChanges();
                                
                        if (tt.TargetTid > 0)
                        {
                            // get last saved object
                            var x = from l in db.Tournements
                                    where l.Tid == t.Tid
                                    select l;
                            // insert into Target Tournement Table.
                            tt.TournementId = x.First().Id;
                            db.TargetTournements.Add(tt);
                        }
                        db.SaveChanges();
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        error = true;
                        //throw;
                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                        error = true;
                    }
                }
                // Console.ReadLine();
            }
            // needs to be done outside of the using so the file is avalble for reading.
            MoveFile(error, path);
        }

        private void MoveFile(bool Errored, string path)
        {
            string Filename = Path.GetFileName(path);
            if (Errored)
            {
                // Move the file.
                File.Move(path, ErrorLocation + Filename);
                Console.WriteLine("{0} was moved to {1}.", path, ErrorLocation + Filename);
            }
            else
            {
                // Move the file.
                File.Move(path, ProcessedLocation + Filename);
                Console.WriteLine("{0} was moved to {1}.", path, ProcessedLocation + Filename);
            }

        }
    }
}
