//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Logic
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tournement
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Tournement()
        {
            this.TargetTournements = new HashSet<TargetTournement>();
        }
    
        public int Id { get; set; }
        public decimal Tid { get; set; }
        public int Players { get; set; }
        public decimal Buyin { get; set; }
        public decimal Rake { get; set; }
        public string Currency { get; set; }
        public int FinishPosition { get; set; }
        public decimal TotalWinnings { get; set; }
        public decimal TotalPrizePool { get; set; }
        public System.DateTime PlayedDate { get; set; }
        public bool IsTicketed { get; set; }
        public bool IsFreeroll { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TargetTournement> TargetTournements { get; set; }
    }
}
